FROM teamatldocker/confluence:7.5.0

USER root

RUN sed -i 's/<session-timeout>300<\/session-timeout>/<session-timeout>0<\/session-timeout>/g' /opt/atlassian/confluence/confluence/WEB-INF/web.xml

USER confluence
CMD ["confluence"]
